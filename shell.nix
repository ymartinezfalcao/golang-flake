{ pkgs ? (
    let
      inherit (builtins) fetchTree fromJSON readFile;
      inherit ((fromJSON (readFile ./flake.lock)).nodes) nixpkgs gomod2nix;
    in
    import (fetchTree nixpkgs.locked) {
      overlays = [
        (import "${fetchTree gomod2nix.locked}/overlay.nix")
      ];
    }
  )
}:

let
  goEnv = pkgs.mkGoEnv { pwd = ./.; };
in
pkgs.mkShell {
  TEMPLATES_LOCATION = "pkg/templates/index.gohtml";
  packages = [
    goEnv
    pkgs.gomod2nix
    pkgs.gopls
    pkgs.godef
    pkgs.golangci-lint
    pkgs.gotools
    pkgs.gore
    pkgs.gocode
    pkgs.gotests
    pkgs.gci
  ];
}
