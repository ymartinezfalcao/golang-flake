{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        gotest = pkgs.writeTextFile {
          name = "gotest";
          destination = "/bin/gotest";
          executable = true;
          text = ''
            go test -v -cover -coverprofile=coverage.out $ARGS ./...
          '';
        };
      in
      {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [ graphviz go gopls golangci-lint go-swagger gotest bombardier gomodifytags ];
        };
      });
}
